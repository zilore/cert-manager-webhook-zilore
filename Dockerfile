FROM golang:1.20-alpine as builder

RUN apk update && apk add --no-cache git ca-certificates && update-ca-certificates
WORKDIR /go/src/gitlab.com/zilore/cert-manager-webhook-zilore

COPY go.mod go.sum ./
RUN go mod download
COPY main.go main.go

RUN CGO_ENABLED=0 go build -trimpath -a -o cert-manager-webhook-zilore main.go

FROM scratch
WORKDIR /
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=builder /go/src/gitlab.com/zilore/cert-manager-webhook-zilore/cert-manager-webhook-zilore .
ENTRYPOINT ["/cert-manager-webhook-zilore"]
