.PHONY: build

build:
	docker build . --platform=linux/amd64 -t registry.gitlab.com/zilore/cert-manager-webhook-zilore:latest
