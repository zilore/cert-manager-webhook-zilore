package main

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"strings"

	"github.com/cert-manager/cert-manager/pkg/acme/webhook/apis/acme/v1alpha1"
	"github.com/cert-manager/cert-manager/pkg/acme/webhook/cmd"
	"gitlab.com/zilore/go-sdk/zilore"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
)

func main() {
	group := os.Getenv("GROUP_NAME")
	if group == "" {
		panic("GROUP_NAME must be specified")
	}
	cmd.RunWebhookServer(group, &ProviderSolver{})
}

type ProviderSolver struct {
	client kubernetes.Interface
}

func (p *ProviderSolver) Name() string { return "zilore" }

func (p *ProviderSolver) Present(ch *v1alpha1.ChallengeRequest) error {
	return p.ToggleRecord(ch, true)
}
func (p *ProviderSolver) CleanUp(ch *v1alpha1.ChallengeRequest) error {
	return p.ToggleRecord(ch, false)
}

func (p *ProviderSolver) ToggleRecord(ch *v1alpha1.ChallengeRequest, present bool) error {
	var (
		domain = DomainName(ch.ResolvedZone)
		record = &zilore.Record{
			Name:  DomainName(ch.ResolvedFQDN),
			Type:  zilore.RecordTypeTXT,
			TTL:   600,
			Value: fmt.Sprintf(`"%s"`, ch.Key),
		}
		config = &Config{}
	)
	err := json.Unmarshal(ch.Config.Raw, config)
	if err != nil {
		return err
	}
	secret, err := p.client.CoreV1().Secrets(ch.ResourceNamespace).Get(context.Background(), config.AuthKey.LocalObjectReference.Name, metav1.GetOptions{})
	if err != nil {
		return err
	}
	key := string(secret.Data[config.AuthKey.Key])
	if key == "" {
		return errors.New("no apiKeySecretRef is provided")
	}
	client := zilore.New(&zilore.Config{
		AuthProvider: zilore.NewAuthKeyProvider(key),
	})
	zone := client.DNS().Domain(domain)
	records, err := zone.ListRecords(&zilore.ListOptions{
		Search: record.Name,
	})
	if err != nil {
		return err
	}
	for _, r := range records {
		if r.Name != record.Name || r.Type != record.Type {
			continue
		}
		if present && r.Value == record.Value {
			return nil
		}
		if err := zone.DeleteRecord(r.ID); err != nil {
			return err
		}
	}
	if !present {
		return nil
	}
	return zone.AddRecord(record)
}

func (p *ProviderSolver) Initialize(config *rest.Config, stop <-chan struct{}) error {
	client, err := kubernetes.NewForConfig(config)
	if err != nil {
		return fmt.Errorf("failed to get kubernetes client: %w", err)
	}
	p.client = client
	return nil
}

type Config struct {
	AuthKey *v1.SecretKeySelector `json:"apiKeySecretRef,omitempty"`
}

func DomainName(s string) string {
	return strings.Trim(s, ".")
}
