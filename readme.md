# Cert-Manager Webhook for Zilore

ACME [webhook](https://cert-manager.io/docs/configuration/acme/dns01/webhook/)
for [cert-manager](https://cert-manager.io/)
allowing users to use [Zilore DNS](https://zilore.com/en/dns) for DNS01 challenge.

### Prerequisites

- A [Zilore API Key](https://my.zilore.com/account/api)
- A valid domain configured on [Zilore DNS](https://zilore.com/en/dns)
- A Kubernetes cluster
- [Helm 3 installed](https://helm.sh/docs/intro/install/) on your computer
- cert-manager [deployed](https://cert-manager.io/docs/installation/) on the cluster

### Installing

```bash
git clone https://gitlab.com/zilore/cert-manager-webhook-zilore.git
make build
helm install cert-manager-webhook-zilore deploy/cert-manager-webhook-zilore
```

### How to use it

```bash
kubectl create -f example/secret.yaml        # Edit the file and specify Zilore API Key
kubectl create -f example/issuer.yaml        # Specify your email
kubectl create -f example/certificate.yaml   # Specify domain names 
kubectl get certificate                      # Check if certificate was issued
```
